#!/usr/bin/python3

import os
import sys
import json
import pydoc
import requests
import argparse

import getch

sys.path.append('/home/seb/MEGA/Python/kodiremote/')

from Mode import Mode

KEYCODE_ESC = 27
KEYCODE_RETRUN = 10
KEYCODE_BACKSPACE = 127


class Remote (object):
    def __init__(self):
        self.url = self.__set_url()
        self.mode = Mode(self, 'default')
        self.__is_running = True
        self.__test()
        self.__set_url()

    def __test(self):
        if requests.post(self.url).status_code != 200:
            raise Exception("Connection error")

    def __set_url(self):
        parser = argparse.ArgumentParser()
        parser.add_argument(
            '--host',
            type=str,
            help="Your KODI webserver address",
            required=False
        )
        parser.add_argument(
            '--port',
            type=str,
            help="Kodi webserver port",
            required=False
        )
        args = vars(parser.parse_args())
        host = args['host']
        port = args['port']
        host = host if host else "http://localhost"
        host = host if host.startswith("http://") else "http://"+host
        port = port if port else "80"
        return "{host}:{port}/jsonrpc".format(host=host, port=port)

    def post(self, jsonrpc="2.0", params={}, **data):
        data['jsonrpc'] = jsonrpc
        data['params'] = params
        headers = {'Content-Type':'application/json'}
        response = requests.post(self.url, json=data, headers=headers)
        return response.json()

    def get_active_player(self):
        return self.post(method="Player.GetActivePlayers", id=0)

    def is_playing(self):
        return True if self.get_active_player()['result'] else False

    def input_action(self, action):
        return self.post(
            method="Input.ExecuteAction",
            params={"action":action},
            id=0
        )

    def send_text(self, text):
        return self.post(
            method="Input.SendText",
            params={'text':text},
            id=0
        )

    def reboot_system(self):
        req_obj = {"jsonrpc": "2.0", "id":1, "method":"System.Reboot", "params":{}}
        return self.post(**req_obj)

    def list_input_actions(self):
        jsonrpc = requests.get(self.url).json()
        text = ''
        for action in jsonrpc['types']['Input.Action']['enums']:
            text += action + '\n'
        pydoc.pager(text)

    def quit(self):
        Mode.clear()
        sys.exit()

    def run_command(self, cmd):
        commands = {
            'q':self.quit,
            'quit':self.quit,
            'reboot':self.reboot_system,
            'reload':self.mode.reload,
            'default':lambda:self.mode.switch('default'),
            'player':lambda:self.mode.switch('player'),
            'command':lambda:self.mode.switch('command'),
            'insert':lambda:self.mode.switch('insert'),
            'volume':lambda:self.mode.switch('volume'),
            'list':self.list_input_actions
        }.get(cmd, lambda: None)()

    def main_loop(self):
        self.mode.run()

if __name__ == "__main__":
    remote = Remote()
    remote.main_loop()
