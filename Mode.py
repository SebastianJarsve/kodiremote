#!/usr/bin/python3

import os
import sys
import time
import json

import getch


class Mode (object):
    def __init__(self, parent, mode):
        self.parent = parent
        self.mode = mode
        self.__mode_data = self.__get_mode_data()
        self.__is_running = False

    def __get_mode_data(self):
        with open(self.mode+'.json') as f:
            data = json.load(f)
            for k, v in data.items():
                self.__setattr__(k, v)
            return data

    def __print(self, text):
        sys.stdout.write(text)
        sys.stdout.flush()

    @classmethod
    def clear(self):
        os.system('cls' if os.name == 'nt' else 'clear')

    def getch(self):
        c = getch.getch()
        return c

    def getln(self):
        text = ''
        c = '0'
        while ord(c) != 27:
            c = getch.getch()
            if ord(c) == 10:
                return text
            elif ord(c) == 127:
                if text:
                    self.__print("\b \b")
                    text = text[:-1]
                    continue
            self.__print(c)
            text += c
        return None

    def reload(self):
        self.clear()
        self.__is_running = True
        cmd = ''
        self.__print(self.prompt)

    def switch(self, mode):
        self.mode = mode
        self.__mode_data = self.__get_mode_data()
        self.reload()

    def run(self):
        self.reload()
        while self.__is_running:
            if self.input_type == 'getln':
                cmd = self.getln()
                if self.mode == 'insert':
                    if cmd:
                        self.parent.send_text(cmd)
                elif self.mode == 'command':
                    if cmd in self.local.values():
                        self.parent.run_command(cmd)
                        # print(cmd)
                        # time.sleep(1)
                    else:
                        self.parent.input_action(cmd)
                self.switch('default')
            elif self.input_type == 'getch':
                cmd = self.getch()
                if cmd in self.local:
                    self.parent.run_command(self.local[cmd])
                elif cmd in self.rpc:
                    self.parent.input_action(self.rpc[cmd])
